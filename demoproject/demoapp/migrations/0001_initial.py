# Generated by Django 4.2.1 on 2023-05-10 20:07

from django.db import migrations, models
import django.db.models.deletion
import wagtail_sb_codefield.fields


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("wagtailcore", "0083_workflowcontenttype"),
    ]

    operations = [
        migrations.CreateModel(
            name="SamplePage",
            fields=[
                (
                    "page_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="wagtailcore.page",
                    ),
                ),
                (
                    "sample_code_field",
                    wagtail_sb_codefield.fields.CodeField(
                        blank=True,
                        help_text="This is a sample code field.",
                        null=True,
                        verbose_name="Sample Code Field",
                    ),
                ),
                (
                    "sample_code",
                    wagtail_sb_codefield.fields.CodeField(
                        blank=True,
                        help_text="This is a sample code field.",
                        null=True,
                        verbose_name="Sample Code",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
            bases=("wagtailcore.page",),
        ),
        migrations.CreateModel(
            name="SampleSnippet",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "name",
                    models.CharField(
                        blank=True,
                        help_text="This is a sample name field.",
                        max_length=255,
                        null=True,
                        verbose_name="Name",
                    ),
                ),
                (
                    "sample_code_field",
                    wagtail_sb_codefield.fields.CodeField(
                        blank=True,
                        help_text="This is a sample code field.",
                        null=True,
                        verbose_name="Sample Code Field",
                    ),
                ),
                (
                    "sample_code",
                    wagtail_sb_codefield.fields.CodeField(
                        blank=True,
                        help_text="This is a sample code field.",
                        null=True,
                        verbose_name="Sample Code",
                    ),
                ),
            ],
        ),
    ]
