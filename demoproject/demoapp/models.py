from django.db import models

from wagtail.models import Page
from wagtail.admin.panels import FieldPanel
from wagtail.snippets.models import register_snippet

from wagtail_sb_codefield.fields import CodeField


class SamplePage(Page):
    sample_code_field = CodeField(
        verbose_name="Sample Code Field",
        blank=True,
        null=True,
        help_text="This is a sample code field.",
        modes=[
            "python",
        ],
        options={
            "theme": "monokai",
            "lineNumbers": True,
        },
    )

    sample_code = CodeField(
        verbose_name="Sample Code",
        blank=True,
        null=True,
        help_text="This is a sample code field.",
        modes=[
            "javascript",
        ],
        options={
            "theme": "monokai",
            "lineNumbers": True,
        },
    )

    content_panels = Page.content_panels + [
        FieldPanel("sample_code_field"),
    ]

    promote_panels = Page.promote_panels + [
        FieldPanel("sample_code"),
    ]


@register_snippet
class SampleSnippet(models.Model):
    name = models.CharField(
        verbose_name="Name",
        max_length=255,
        blank=True,
        null=True,
        help_text="This is a sample name field.",
    )

    sample_code_field = CodeField(
        verbose_name="Sample Code Field",
        blank=True,
        null=True,
        help_text="This is a sample code field.",
        modes=[
            "python",
        ],
        options={
            "theme": "monokai",
            "lineNumbers": True,
        },
    )

    sample_code = CodeField(
        verbose_name="Sample Code",
        blank=True,
        null=True,
        help_text="This is a sample code field.",
        modes=[
            "javascript",
        ],
        options={
            "theme": "monokai",
            "lineNumbers": True,
        },
    )

    panels = [
        FieldPanel("name"),
        FieldPanel("sample_code_field"),
        FieldPanel("sample_code"),
    ]
