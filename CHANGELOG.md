# Changelog

## [Unreleased]

## [0.4.0] - 2023-05-10

* Add support for wagtail 5.x

## [0.3.0] - 2023-04-29

* Update dependency declaration on `django-sb-codefield`

## [0.2.0] - 2023-04-29

* Update package description in README.md and pyproject.toml
* Updatecodacy badge in README.md

## [0.1.0] - 2023-04-29

* Wagtail admin compatibility
* Configured demo project
