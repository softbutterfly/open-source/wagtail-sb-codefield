from django.apps import AppConfig


class WagtailSbCodefieldConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "wagtail_sb_codefield"
